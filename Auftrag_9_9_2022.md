# Checklisten des Datenschutzbeauftragten des Kantons Zürichs

# Checkliste Smartphone-Sicherheit erhöhen
 - Link: https://www.datenschutz.ch/meine-daten-schuetzen/smartphone-sicherheit-erhoehen
 - -----
 - 1. Gerätesperre aktivieren
   - Ja ich habe Gesichtserkennung und Muster aktiviert.
   - ---------
- 2. Gerät bei Verlust sofort sperren und löschen lassen
  - Ja ich habe die Remote-Wipe-Funktion bereits eingerichtet, aber zum Glück bis heute nicht gebraucht.
  - ---------
- 3. Apps aus vertauenswürdigen Quellen installieren
  -  Ich installiere nur Apps aus dem Google Play Store, die von Play Protect überprüft wurden.
  -  ------
- 4. Öffentliche Funknetzwerke mit Vorsicht benutzen
  - Ich benutze öffentliche Netzwerke nur im allergrössten Notfall oder wenn ich weiss, dass sie sicher sind.
  - -------
-   5. Updates regelmässig durchführen
    -   Ich erhalte immer Benachrichtigungen falls ein Software- oder Systemupdate verfügbar ist.   
  -----------
  - 6. Daten vor Verkauf oder Entsorgung komplett löschen
    - Ich habe bisher mein Handy nie entsorgt oder verkauft. Deswegen brauchte ich mein Handy noch nie zuürcksetzen.
    - -----
- 7. Drahtlose Schnittstellen deaktivieren
   - Sobald ich kein WLAN oder Bluetooth brauche deaktiviere ich es meistens. Eben auch unter Anderem wegen dem Akkuverbrauch. 
  




  # PC-Sicherheit erhöhen in 5 Schritten
 - Link:https://www.datenschutz.ch/meine-daten-schuetzen/pc-sicherheit-erhoehen-in-fuenf-schritten
 - ------
- 1. Persönliche Informationen
  - Ich benutze Opera GX hauptsächlich als Internetbrowser und habe es so eingerichtet, dass es den Cache automatisch löscht wenn die Software geschlossen wird. Deswegen bin ich auch geschützt, wenn ich mal nicht daran denke.
  - ------
  
- 2. Angriffe abwehren
  -  Ich habe die Firewall aktiviert und benutze Windows Defender als Antivirenschutzprogramm
  -------
- 3. Zugriffe Unberechtigter verhindern
  - Ich verwende Passwörter die nur ich kenne. Ich habe bei all meinen Smart-Geräten einen Lock Screen eingerichtet damit niemand ausser ich Zugriff drauf habe.
  - ------
- 4. Verschlüsseln Sie sensitive Inhalte
  - Ich habe einen Verschlüssler installiert der all meine Daten verschlüsselt.
  - -------
- 5.  Sichern Sie Informationen und löschen Sie Daten vollständig
  - Ich habe mich bei GDrive und Onedrive angemeldet. Diese machen regelmässig ein Backup.